package com.techu.apimarket.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "autorizador")
public class AutorizadorModel {

    @Id
    private String idCliente;
    private List<CuentasModel> cuentas;
    private int ultimaAutorizacion;

    public AutorizadorModel() {
    }

    public AutorizadorModel(String idCliente, List<CuentasModel> cuentas, int ultimaAutorizacion) {
        this.idCliente = idCliente;
        this.cuentas = cuentas;
        this.ultimaAutorizacion = ultimaAutorizacion;
    }

    public String getIdCliente() {
        return this.idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public List<CuentasModel> getCuentas() {
        return this.cuentas;
    }

    public void setCuentas(List<CuentasModel> cuentas) {
        this.cuentas = cuentas;
    }

    public int getUltimaAutorizacion() {
        return this.ultimaAutorizacion;
    }

    public void setUltimaAutorizacion(int ultimaAutorizacion) {
        this.ultimaAutorizacion = ultimaAutorizacion;
    }
}

