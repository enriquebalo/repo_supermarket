package com.techu.apimarket.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "purchases")
public class PurchaseModel {

    @Id
    private String id;
    private String idClient;
    private Map<String,Integer> products;
    private float totalAmount;
    private String payment;
    private int folio;

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String idClient, Map<String, Integer> products, float totalAmount, String payment, int folio) {
        this.id = id;
        this.idClient = idClient;
        this.products = products;
        this.totalAmount = totalAmount;
        this.payment = payment;
        this.folio = folio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public Map<String, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<String, Integer> products) {
        this.products = products;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public int getFolio() {
        return this.folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    @Override
    public String toString() {
        return "PurchaseModel{" +
                ", No. de cliente ='" + idClient + '\'' +
                ", Productos =" + products +
                ", Total=" + totalAmount +
                ", Forma de pago='" + payment + '\'' +
                ", Folio compra =" + folio +
                '}';
    }
}
