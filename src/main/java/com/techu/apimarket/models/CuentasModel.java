package com.techu.apimarket.models;


public class CuentasModel {

    private String noCuenta;
    private String tipo;
    private float saldo;

    public CuentasModel() {
    }

    public CuentasModel(String noCuenta, String tipo, float saldo) {
        this.noCuenta = noCuenta;
        this.tipo = tipo;
        this.saldo = saldo;
    }

    public String getNoCuenta() {
        return noCuenta;
    }

    public void setNoCuenta(String noCuenta) {
        this.noCuenta = noCuenta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
}
