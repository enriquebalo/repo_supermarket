package com.techu.apimarket.models;

        import org.springframework.data.annotation.Id;
        import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "productMarket")
public class Mproduct {

    @Id
    private String idpm;
    private String descM;
    private String marcaM;
    private float precioM;
    private String unidadM;
    public Mproduct() {
    }
    public Mproduct(String idpm, String descM, String marcaM, float precioM, String unidadM) {
        this.idpm = idpm;
        this.descM = descM;
        this.marcaM = marcaM;
        this.precioM = precioM;
        this.unidadM = unidadM;
    }
    public String getIdpm() {
        return this.idpm;
    }

    public void setIdpm(String idpm) {
        this.idpm = idpm;
    }

    public String  getDescM() {
        return this.descM;
    }

    public void  setDescM(String descM) {
        this.descM = descM;
    }
    public  String getMarcaM() {
        return this.marcaM ;
    }
    public void  setMarcaM(String marcaM) {
        this.marcaM = marcaM;
    }

    public float getPrecioM() {
        return this.precioM;
    }
    public void setPrecioM(float precioM) {
        this.precioM= precioM;
    }

    public  String getUnidadM() {
        return this.unidadM;
    }
    public void setUnidadM(String unidadM) {
        this.unidadM = unidadM;
    }


}
