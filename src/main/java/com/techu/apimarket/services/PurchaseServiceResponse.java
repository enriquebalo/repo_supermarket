package com.techu.apimarket.services;

import com.techu.apimarket.models.PurchaseModel;
import org.springframework.http.HttpStatus;

public class PurchaseServiceResponse {

    private String msg;
    private PurchaseModel purchase;
    private HttpStatus httpStatus;

    public PurchaseServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PurchaseModel getPurchase() {
        return purchase;
    }

    public void setPurchase(PurchaseModel purchase) {
        this.purchase = purchase;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
