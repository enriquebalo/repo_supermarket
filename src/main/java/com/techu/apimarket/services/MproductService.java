package com.techu.apimarket.services;

import com.techu.apimarket.models.Mproduct;
import com.techu.apimarket.repositories.MproductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MproductService {
    @Autowired
    MproductRepository mproductRepository;

    public List<Mproduct> findAll() {
        System.out.println("Final Mproduct service");
        return this.mproductRepository.findAll();

    }

    public Mproduct add(Mproduct mproduct) {
        System.out.println("agregar un producto en el product service");

        return this.mproductRepository.save(mproduct);
    }

    public Optional<Mproduct> findById(String id) {
        System.out.println("findById de productService ");
        return this.mproductRepository.findById(id);
    }

    public Mproduct update(Mproduct mproduct) {
        System.out.println("update en ProductService");

        return this.mproductRepository.save(mproduct);


    }


    public boolean delete(String id) {
        System.out.println("eliminar producto en el productService");

        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("producto eliminado del market ");
            this.mproductRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
