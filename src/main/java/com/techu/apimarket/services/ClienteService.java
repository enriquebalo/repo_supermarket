package com.techu.apimarket.services;

import com.techu.apimarket.models.ClienteModel;
import com.techu.apimarket.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public List<ClienteModel> findAll() {
        System.out.println("find UserService");

        return this.clienteRepository.findAll();
    }

    public ClienteModel add(ClienteModel user){
        System.out.println("add en userService");

        return this.clienteRepository.save(user);
    }
    public Optional<ClienteModel> findById(String id){
        System.out.println("En findById de ClienteService");

        return this.clienteRepository.findById(id);
    }

    public ClienteModel update(ClienteModel clienteModel){
        System.out.println("Update en ClienteService");

        return this.clienteRepository.save(clienteModel);
    }

    public boolean delete(String id){
        System.out.println("delete en ClienteService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Cliente encontrado borrado");
            this.clienteRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
