package com.techu.apimarket.services;

import com.techu.apimarket.models.AutorizadorModel;
import org.springframework.http.HttpStatus;

public class AutorizadorServiceResponse {

    private String message;
    private AutorizadorModel autorizador;
    private HttpStatus codeStatus;

    public AutorizadorServiceResponse() {
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AutorizadorModel getAutorizador() {
        return this.autorizador;
    }

    public void setAutorizador(AutorizadorModel autorizador) {
        this.autorizador = autorizador;
    }

    public HttpStatus getCodeStatus() {
        return this.codeStatus;
    }

    public void setCodeStatus(HttpStatus codeStatus) {
        this.codeStatus = codeStatus;
    }
}

