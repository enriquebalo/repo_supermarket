package com.techu.apimarket.services;

import com.techu.apimarket.models.AutorizadorModel;
import com.techu.apimarket.repositories.AutorizadorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AutorizadorService {

    @Autowired
    AutorizadorRepository autorizadorRepository;

    @Autowired
    ClienteService clienteService;

    public AutorizadorServiceResponse addAutorizador(AutorizadorModel autorizadorModel){
        System.out.println("addAutorizador en AutorizadorService");

        AutorizadorServiceResponse result = new AutorizadorServiceResponse();
        result.setAutorizador(autorizadorModel);
        if(this.clienteService.findById(autorizadorModel.getIdCliente()).isEmpty() == true){
            System.out.println("El cliente que desea comprar no existe");
            result.setMessage("Cliente no encontrado");
            result.setCodeStatus(HttpStatus.BAD_REQUEST);

            return result;
        }
        this.autorizadorRepository.save(autorizadorModel);
        result.setMessage("Autorizador añadido exitosamente");
        result.setCodeStatus(HttpStatus.CREATED);
        return result;
    }

    public boolean verificaSaldo(AutorizadorModel autorizadorModel, float importe, String tipo){
        boolean bandSaldo = false;
        /*Recorre la lista de cuentas*/
        for(int i = 0; autorizadorModel.getCuentas().size() > i; i++){
            /*Verifica que el tipo de cuenta exista en la lista del cliente*/
            if(autorizadorModel.getCuentas().get(i).getTipo().equals(tipo)){
                /*Verifica que el importe de la cuenta sea suficiente*/
                if(autorizadorModel.getCuentas().get(i).getSaldo() > importe){
                    System.out.println("Se puede realizar la compra. Haciendo cargo...");
                    float saldoNuevo = autorizadorModel.getCuentas().get(i).getSaldo() - importe;
                    autorizadorModel.getCuentas().get(i).setSaldo(saldoNuevo);
                    int auto = autorizadorModel.getUltimaAutorizacion() + 1;
                    autorizadorModel.setUltimaAutorizacion(auto);
                    /*Si el pago es con TDC/TDD -> Se suman puntos a MON*/
                    if(!tipo.equals("MON")){
                        /*Vuelvo a recorrer la lista del cliente en busca de monedero*/
                        for(int j = 0; autorizadorModel.getCuentas().size() > j; j++){
                            if(autorizadorModel.getCuentas().get(j).getTipo().equals("MON")){
                                if(tipo.equals("TDC") || tipo.equals("TDD")){
                                    float puntos = (importe*10)/100;
                                    float saldoPuntos = autorizadorModel.getCuentas().get(j).getSaldo() + puntos;
                                    autorizadorModel.getCuentas().get(j).setSaldo(saldoPuntos);
                                    this.autorizadorRepository.save(autorizadorModel);
                                }
                            }else{
                                this.autorizadorRepository.save(autorizadorModel);
                            }
                        }
                    }
                    else{
                        /*Solo se hace el pago con MON y disminuyen los puntos*/
                        this.autorizadorRepository.save(autorizadorModel);
                    }
                    bandSaldo = true;
                }
            }
        }
        return bandSaldo;
    }

    public Optional<AutorizadorModel> getAutorizador(String id){
        System.out.println("getAutorizador en AutorizadorService");

        return this.autorizadorRepository.findById(id);

    }
}

