package com.techu.apimarket.services;

import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonArrayFormatVisitor;
import com.techu.apimarket.models.AutorizadorModel;
import com.techu.apimarket.models.CuentasModel;
import com.techu.apimarket.models.PurchaseModel;
import com.techu.apimarket.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    ClienteService clienteService;

    @Autowired
    AutorizadorService autorizadorService;

    @Autowired
    MproductService productService;

    @Autowired
    EmailSenderService senderService;

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase){
        System.out.println("addPurchaseService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if(this.clienteService.findById(result.getPurchase().getIdClient()).isEmpty()){
            System.out.println("El cliente no ha sido encontrado");

            result.setMsg("Cliente no encontrado");
            result.setHttpStatus(HttpStatus.NOT_FOUND);
            return result;
        }

        if (this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con esa id");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getProducts().entrySet()) {
            if (this.productService.findById(purchaseItem.getKey()).isEmpty()) {
                System.out.println("El producto con la id " + purchaseItem.getKey()
                        + " no se encuentra en el sistema");

                result.setMsg("El producto con la id " + purchaseItem.getKey()
                        + " no se encuentra en el sistema");
                result.setHttpStatus(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItem.getValue()
                        + " unidades del producto al total");

                amount +=
                        (this.productService.findById(purchaseItem.getKey()).get().getPrecioM()
                                * purchaseItem.getValue());
            }
        }

        purchase.setTotalAmount(amount);

        Optional<AutorizadorModel> autorizador = this.autorizadorService.getAutorizador(purchase.getIdClient());
        String tipo = purchase.getPayment();

        if(autorizador.isEmpty()){
            System.out.println("No existe cuenta");

            result.setMsg("Cuenta del cliente no existe");
            result.setHttpStatus(HttpStatus.NOT_FOUND);
            return result;
        }

        if(this.autorizadorService.verificaSaldo(autorizador.get(), amount, tipo)== false){
            System.out.println("No cuenta con saldo suficiente");

            result.setMsg("No cuenta con saldo suficiente");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

        purchase.setFolio(autorizador.get().getUltimaAutorizacion());
        purchaseRepository.save(purchase);
        senderService.senEmail("enriquebalo2@gmail.com","Recibo de compra",purchase.toString());
        result.setMsg("La compra ha sido agregada correctamente");
        result.setHttpStatus(HttpStatus.CREATED);
        return result;
    }
    //@EventListener(ApplicationReadyEvent.class)
    /*public void sendMail(PurchaseModel purchase) {
        senderService.senEmail("enriquebalo2@gmail.com","Recibo de compra",purchase.toString());

    }
*/
    public List<PurchaseModel> getPurchases(){
        System.out.println("getPurchases Service");

        return this.purchaseRepository.findAll();
    }

}