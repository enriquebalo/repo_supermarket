package com.techu.apimarket.controllers;


import com.techu.apimarket.models.Mproduct;
import com.techu.apimarket.services.MproductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController

@RequestMapping("/apimarket/v1")
public class MproductController {

    @Autowired
    MproductService mproductService;


    @GetMapping("/MarketProducts")
    public ResponseEntity<List<Mproduct>> getMproduct() {
        System.out.println("getMproduct");

        return new ResponseEntity<>(
                this.mproductService.findAll(),
                HttpStatus.OK
        );

    }

    @PostMapping("/MarketProducts")
    public ResponseEntity<Mproduct> addMproduct(@RequestBody Mproduct mproduct) {
        System.out.println("add Product Market");
        System.out.println("El id del producto que se va a crear es " + mproduct.getIdpm());
        System.out.println("La descripción del producto a agregar es " + mproduct.getDescM());
        System.out.println("El precio unitario del producto que se va a agregar es" + mproduct.getPrecioM());
        System.out.println("La marca del producto que se va a agregar es" + mproduct.getMarcaM());
        System.out.println("La unidad de medida del que se va a agregar es" + mproduct.getUnidadM());

        return new ResponseEntity<>(
                this.mproductService.add(mproduct),
                HttpStatus.CREATED
        );


    }

    @GetMapping("/MarketProducts/{id}")
    public ResponseEntity<Object> getMproductById(@PathVariable String id) {
        System.out.println("getProductoById");
        System.out.println("El id del producto es " + id);

        Optional<Mproduct> result = this.mproductService.findById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "por el momento no contamos con este producto",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/MarketProducts/{id}")
    public ResponseEntity<Mproduct> updateMproduct(@RequestBody Mproduct product, @PathVariable String id) {
        System.out.println("Actualizando un producto ");
        System.out.println("El ID a actualizar es " + product.getIdpm() );
        System.out.println("La descripción a actualizar es " + product.getDescM());
        System.out.println("El precio a actualizar es "  + product.getPrecioM());
        System.out.println("La marca  a actualizar es " + product.getMarcaM());
        System.out.println("La marca  a actualizar es " + product.getUnidadM());

        Optional<Mproduct> result = this.mproductService.findById(id);

        Optional<Mproduct> productToUpdate = this.mproductService.findById(id);
        if (productToUpdate.isPresent()) {
            System.out.println("producto a actualizar localizado. actualizando ");
            this.mproductService.update(product);
        }

        return new ResponseEntity<>(
                product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
        @DeleteMapping("/MarketProducts/{id}")
        public ResponseEntity<String> deleteMProduct(@PathVariable String id){
            System.out.println("Eliminar productor del Market");

            boolean deletedProduct = this.mproductService.delete(id);
            return new ResponseEntity<>(
                    deletedProduct ? "producto eliminado del market": "producto no eliminado del market",
                    deletedProduct ? HttpStatus.OK: HttpStatus.NOT_FOUND
            );

    }
}


