package com.techu.apimarket.controllers;

import com.techu.apimarket.models.ClienteModel;
import com.techu.apimarket.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apimarket/v1")
public class ClienteController <cliente> {

    @Autowired
    ClienteService clienteService;


    @GetMapping("/cliente")
    public ResponseEntity<List<ClienteModel>> getclientes(){
        System.out.println("getclientes");

        return new ResponseEntity<>(
                this.clienteService.findAll()
                , HttpStatus.OK
        );
    }

    @PostMapping("/cliente")
    public ResponseEntity<ClienteModel> addProduct(@RequestBody ClienteModel cliente){
        System.out.println("addCliente");
        System.out.println("La id del cliente que se vaa crear es " + cliente.getId());
        System.out.println("EL nombre del cliente que se va a crear es "+ cliente.getNombre());


        return new ResponseEntity<>(
                this.clienteService.add(cliente)
                , HttpStatus.CREATED
        );

    }

    @GetMapping("/cliente/{id}")
    public ResponseEntity<Object> getClienteByid(@PathVariable String id){
        System.out.println("getClienteByid");
        System.out.println("La id del cliente a buscar es " + id);

        Optional<ClienteModel> result = this.clienteService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "User no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/cliente/{id}")
    public ResponseEntity<ClienteModel> UpdateCliente(@RequestBody ClienteModel cliente, @PathVariable String id){
        System.out.println("UpdateCliente");
        System.out.println("La id del cliente que se va a ctualizar en parametro URL es es " + id);
        System.out.println("La id del cliente que se va a actualizar es " + cliente.getId() );
        System.out.println("El nombre del usuario que se va a actualizar es "+cliente.getNombre());


        Optional<ClienteModel> ClienteToUpdate = this.clienteService.findById(id);

        if(ClienteToUpdate.isPresent()){
            System.out.println("Cliente para actualizar encontrado , actualizando");
            this.clienteService.update(cliente);
        }

        return new ResponseEntity<>(
                cliente
                , ClienteToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/cliente/{id}")
    public ResponseEntity<String> deleteCliente (@PathVariable String id){
        System.out.println("deleteCliente");

        boolean deleteUser = this.clienteService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Cliente borrado" : "Usuario no borrado" ,
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
