package com.techu.apimarket.controllers;

import com.techu.apimarket.models.AutorizadorModel;
import com.techu.apimarket.services.AutorizadorService;
import com.techu.apimarket.services.AutorizadorServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apimarket/v1")
public class AutorizadorController {

    @Autowired
    AutorizadorService autorizadorService;

    @PostMapping("/autorizadores")
    public ResponseEntity<AutorizadorServiceResponse> addAutorizador(@RequestBody AutorizadorModel autorizadorModel){
        System.out.println("addAutorizador");
        System.out.println("El identificador del cliente es: "+autorizadorModel.getIdCliente());
        System.out.println("La lista de cuentas es "+autorizadorModel.getCuentas());
        System.out.println("La ultima autorizacion del cliente es "+autorizadorModel.getUltimaAutorizacion());

        AutorizadorServiceResponse result = this.autorizadorService.addAutorizador(autorizadorModel);

        return new ResponseEntity<>(result, result.getCodeStatus());
    }

}

