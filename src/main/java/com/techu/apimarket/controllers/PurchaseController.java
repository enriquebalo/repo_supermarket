package com.techu.apimarket.controllers;

import com.techu.apimarket.models.PurchaseModel;
import com.techu.apimarket.services.PurchaseService;
import com.techu.apimarket.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apimarket/v1")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase");

        PurchaseServiceResponse result = purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                result,
                result.getHttpStatus()
        );
    }

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases");

        return new ResponseEntity<>(

                this.purchaseService.getPurchases(),
                HttpStatus.OK
        );
    }
}
