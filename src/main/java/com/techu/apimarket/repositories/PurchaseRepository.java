package com.techu.apimarket.repositories;

import com.techu.apimarket.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends MongoRepository<PurchaseModel,String> {
}
