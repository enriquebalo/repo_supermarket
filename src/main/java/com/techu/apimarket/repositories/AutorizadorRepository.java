package com.techu.apimarket.repositories;

import com.techu.apimarket.models.AutorizadorModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AutorizadorRepository extends MongoRepository<AutorizadorModel, String> {
}

