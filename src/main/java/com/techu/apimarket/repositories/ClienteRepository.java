package com.techu.apimarket.repositories;

import com.techu.apimarket.models.ClienteModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends MongoRepository<ClienteModel , String> {

}
