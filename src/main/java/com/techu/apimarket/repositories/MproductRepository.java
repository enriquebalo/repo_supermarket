package com.techu.apimarket.repositories;


import com.techu.apimarket.models.Mproduct;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MproductRepository extends MongoRepository<Mproduct, String>{

}
