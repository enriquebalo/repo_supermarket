package com.techu.apimarket;


import com.techu.apimarket.services.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class ApimarketApplication {
@Autowired
EmailSenderService senderService;
	public static void main(String[] args) {
		SpringApplication.run(ApimarketApplication.class, args);

	}

}
